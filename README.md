# Jupyter C Kernel - ZIH Edition

Dies ist ein modifizierter C Kernel für das Komplexpraktikum Paralleles Rechnen am ZIH. Er basiert auf der Version 1.2.2 des C Kernels vom 24 Jan 2018.

## Installation des Kernels

Die Installation muss in einem Taurus Projektverzeichnis geschehen. Jeder Nutzer des Kernels muss Zugriff auf dieses Verzeichnis haben.

Vorbereitung einer Python venv:

* `cd $Projektverzeichnis`
* `module load Python/3.8.6-GCCcore-10.2.0` (ein einfaches *module load Python* wird/kann später fehlschlagen!)
* `virtualenv --system-site-packages c-kernel`
* `cd c-kernel`
* `source bin/activate`
* `pip install --upgrade pip`
* `pip install ipykernel`

Installation des Kernels:
* `https://gitlab.hrz.tu-chemnitz.de/s8966401--tu-dresden.de/jupyter-c-zih-edition.git`
* `cd jupyter-c-zih-edition`
* `python setup.py install` (*pip install .* wird nicht funktionieren.)

Fertig :)

## Linken des Kernels in das eigene home directory

Diese Schritte müssen von jedem Nutzer ausgeführt werden, der mit dem Kernel interagieren möchte.

* `cd $Projektverzeichnis/c-kernel`
* `module load Python/3.8.6-GCCcore-10.2.0 `
* `source bin/activate `
* `install_c_kernel `
* `deactivate`

## Interaktion (magics)

Der Kernel unterstützt magics, um Compiler, Anzahl von Cores und die Verwendung des Batch Systems *Slurm* zu verwenden.

### Wie verwendet man magics?

Am Anfang der Datei müssen Zeilen mit folgendem Anfang eingefügt werden:

`//%`

Daran erkennt der C-Kernel, dass es sich um flags für die Ausführung handelt.

### Welche Flags werden unterstützt?

* cflags und ldflags (Default: None)
* args (Argumente für die Programmausführung)
* compiler (gcc und icc, Default: gcc)
* slurm (setzt alle vorherigen Flags außer Kraft)
* jobname (vergibt dem Job einen Namen, Default: slurm; funktioniert nur in Verbindung mit slurm)

#### Hinweise:

Große Berechnungen sollen via Slurm und nicht auf den Login Servern ausgeführt werden!

### Ein Beispiel:

#### Ohne Slurm

Verwendet die Compiler flags *lm* und *O2*, compiliert mit *icc*.

```C
//%cflags: -lm -O2
//%compiler: icc
```

#### Mit Slurm


```bash
//%jobname:test
/*%slurm
#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-core=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=romeo
#SBATCH --time=01:00:00
#SBATCH --account=*Projektname*

module purge
module load GCC/11.2.0

gcc -O3 test.c -o gof.out
*/
```

Die Verwendung von Jobnames ermöglicht es, mehere Jobs zu starten. Verwendet man den selben oder keinen Jobname mehrfach, wird der Job überschreiben und erneut gescheduled.

**Achtung:** Das kann dazu führen, dass der selbe Job mehrfach ausgeführt wird. Dies gilt es zu vermeiden! AUf der Konsole lässt sich mit `sacct` alle laufenden und geplanten Jobs einsehen.
