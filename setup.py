import os
import sys

from setuptools import setup
from setuptools.command.install import install


class PostInstallCommand(install):
    """Post-installation for installation mode."""

    def run(self):
        install.run(self)
        print("Setting up Jupyter C Kernel for HPC...")
        loader_path = os.path.join(os.environ['VIRTUAL_ENV'], "bin", "loader.sh")
        with open(loader_path, 'w') as f:
            f.write('#!/usr/bin/bash\n')
            f.write('\n')
            f.write('module load Python/3.8.6\n')
            f.write('module load icc\n')
            f.write(sys.executable + ' "$@"\n')
        os.chmod(loader_path, 0o755)
        print("Done.")


setup(name='jupyter_c_kernel',
      version='1.0',
      description='Minimalistic C kernel for Jupyter ZIH edition',
      packages=['jupyter_c_kernel'],
      scripts=['jupyter_c_kernel/install_c_kernel'],
      keywords=['jupyter', 'notebook', 'kernel', 'c'],
      include_package_data=True,
      cmdclass={
          'install': PostInstallCommand,
      },
      )
